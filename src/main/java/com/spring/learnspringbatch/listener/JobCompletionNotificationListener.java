package com.spring.learnspringbatch.listener;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.batch.core.BatchStatus;
import org.springframework.batch.core.JobExecution;
import org.springframework.batch.core.JobExecutionListener;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Component;

import com.spring.learnspringbatch.model.Person;

@Component
public class JobCompletionNotificationListener implements JobExecutionListener {

    private static final Logger LOGGER = LoggerFactory.getLogger(JobCompletionNotificationListener.class);
    private final JdbcTemplate jdbcTemplate;
    private String msg = "!!! JOB FINISHED! Time to verify the results";
    private String sqlString = "SELECT first_name, last_name FROM people";

    /* (non-Javadoc)
     * @see org.springframework.batch.core.JobExecutionListener#afterJob(org.springframework.batch.core.JobExecution)
     */
    @Override
    public void afterJob(JobExecution jobExecution) {
        if(BatchStatus.COMPLETED == jobExecution.getStatus()) {
            LOGGER.info(msg);
      
            jdbcTemplate.query(sqlString,
              (rs, row) -> new Person(
                rs.getString(1),
                rs.getString(2))
            ).forEach(person -> LOGGER.info("Found <" + person + "> in the database."));
        }
    }

    /**
     * Constructor.
     * @param jdbcTemplate
     */
    public JobCompletionNotificationListener(JdbcTemplate jdbcTemplate) {
        this.jdbcTemplate = jdbcTemplate;
    }

    /* (non-Javadoc)
     * @see org.springframework.batch.core.JobExecutionListener#beforeJob(org.springframework.batch.core.JobExecution)
     */
    @Override
    public void beforeJob(JobExecution jobExecution) {
    }
    
}
