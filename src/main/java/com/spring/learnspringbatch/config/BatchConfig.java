package com.spring.learnspringbatch.config;


import javax.sql.DataSource;

import org.springframework.batch.core.Job;
import org.springframework.batch.core.Step;
import org.springframework.batch.core.configuration.annotation.EnableBatchProcessing;
import org.springframework.batch.core.configuration.annotation.JobBuilderFactory;
import org.springframework.batch.core.configuration.annotation.StepBuilderFactory;
import org.springframework.batch.core.launch.support.RunIdIncrementer;
import org.springframework.batch.item.database.BeanPropertyItemSqlParameterSourceProvider;
import org.springframework.batch.item.database.JdbcBatchItemWriter;
import org.springframework.batch.item.database.builder.JdbcBatchItemWriterBuilder;
import org.springframework.batch.item.file.FlatFileItemReader;
import org.springframework.batch.item.file.builder.FlatFileItemReaderBuilder;
import org.springframework.batch.item.file.mapping.BeanWrapperFieldSetMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.io.ClassPathResource;

import com.spring.learnspringbatch.listener.JobCompletionNotificationListener;
import com.spring.learnspringbatch.model.Person;
import com.spring.learnspringbatch.processor.PersonItemProcessor;

@Configuration
@EnableBatchProcessing
public class BatchConfig {

    @Autowired
    public JobBuilderFactory jobBuilderFactory;
    
    @Autowired
    public StepBuilderFactory stepBuilderFactory;
    
    private String sqlString = "INSERT INTO people (first_name, last_name) VALUES (:firstName, :lastName)";
    private String csv = "sample-data.csv";
    private String iteamReaderName = "personItemReader";
    
    /**
     * Creates an ItemReader. It looks for a file called sample-data.csv and 
     * parses each line item with enough information to turn it into a Person.
     * @return ItemReader
     */
    @Bean
    public FlatFileItemReader<Person> reader() {
        return new FlatFileItemReaderBuilder<Person>()
            .name(iteamReaderName)
            .resource(new ClassPathResource(csv))
            .delimited()
            .names(new String[]{"firstName", "lastName"})
            .fieldSetMapper(new BeanWrapperFieldSetMapper<Person>() {{
            setTargetType(Person.class);
            }})
            .build();
    }

    /**
     * Creates an instance of the PersonItemProcessor defined earlier.
     * Meant to convert the data to upper case.
     * @return PersonItemProcessor
     */
    @Bean
    public PersonItemProcessor processor() {
        return new PersonItemProcessor();
    }

    /**
     * Creates an ItemWriter. This one is aimed at a JDBC destination and 
     * automatically gets a copy of the dataSource created by @EnableBatchProcessing. 
     * It includes the SQL statement needed to insert a single Person, 
     * driven by Java bean properties.
     * 
     * @param dataSource
     * @return ItemWriter
     */
    @Bean
    public JdbcBatchItemWriter<Person> writer(DataSource dataSource) {
        return new JdbcBatchItemWriterBuilder<Person>()
            .itemSqlParameterSourceProvider(new BeanPropertyItemSqlParameterSourceProvider<>())
            .sql(sqlString)
            .dataSource(dataSource)
            .build();
    }
    @Bean
    public Job importUserJob(JobCompletionNotificationListener listener, Step step) {
        return jobBuilderFactory.get("importUserJob")
            .incrementer(new RunIdIncrementer())
            .listener(listener)
            .flow(step)
            .end()
            .build();
    }
    
    @Bean
    public Step step(JdbcBatchItemWriter<Person> writer) {

        // LOGGER.info("##### stepBuilderFactory.get(): " + stepBuilderFactory.get("step"));

        return stepBuilderFactory.get("step")
            .<Person, Person> chunk(10)
            .reader(reader())
            .processor(processor())
            .writer(writer)
            .build();
    }
}