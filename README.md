# learn-springbatch

A simple spring batch app to try before implementation.

## Tech
- Spring boot 2.7.1 (1.0.11.RELEASE)
- Gradle
- Java 17
- HSQL DB